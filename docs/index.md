
## Authors
- [Courcelle Maxime](https://github.com/CourcelleM/PPRreads_ASSEMBLY)
- Antoni Exbrayat

<center> <h1>Manuel d’utilisation du script d’assemblage de génomes PPR</h1> </center>

## 1. Connexion au cluster Muse

### Depuis un ordinateur Windows

Il est possible de se connecter directement depuis l’invite de commande (avec la même commande
qu’un ordinateur Linux), mais il est plus simple d’installer un logiciel de gestion de connexions à
distance comme MobaXterm (https://mobaxterm.mobatek.net/download-home-edition.html) ou
putty.

### Depuis un ordinateur sur Linux

Ouvrir le terminal et utiliser la commande
```console
ssh identifiant@muse-login.meso.umontpellier.fr
```

en remplaçant « identifiant » par le nom (login) qui vous a été attribué par les administrateurs de
Meso@LR. Il vous sera ensuite demandé d’entrer votre mot de passe. Une fois connecté, vous aurez
accès à un environnement à distance Linux en ligne de commande. Une bonne documentation, avec
une liste de commandes Linux utiles, est disponible sur le site de Meso@LR : https://mesolr.umontpellier.fr/documentation-utilisateurs/


## 2. Installation du script

Tous les documents et les fichiers nécessaires à l’utilisation du script sont stockés sur le cloud Github
de Maxime Courcelle et peuvent être récupérés facilement via la commande suivante :
```console
git clone - b muse-cluster --single-branch https://gitlab.cirad.fr/astre/PPRreads_ASSEMBLY.git
```
Cela permet à tous les utilisateurs d’avoir à disposition la dernière version de tous les fichiers
nécessaires, et permet de partager le script facilement. N’importe qui peut télécharger
(gratuitement) le contenu de ce github.

### Activation de Snakemake

Snakemake est le gestionnaire qui s’assure que tous les éléments du pipeline s’enchaînent
correctement et dans le bon ordre. Il est pré-installé sur le cluster CIRAD et la plupart des autres,
mais nécessite d’être activé grâce à la commande suivante :
```console
module load snakemake
```
Contrairement aux itérations précédentes du script, c’est également Snakemake qui s’occupera
d’installer tous les outils nécessaires à l’analyse et la configuration est effectuée automatiquement.

## 3. Lancer une analyse

### Copie et format des données brutes

Le script nécessite en entrée deux fichiers de lectures paired-end au format compressé fastq.gz. Le
plus simple est de créer un dossier « data » et d’y déposer les données manuellement via
MobaXterm.

Créer le dossier dans le répertoire que l’on vient de télécharger :
```console
cd PPRreads_ASSEMBLY
mkdir data
```
### Paramètres

Certains paramètres du script doivent être adaptés pour s’adapter aux différents clusters et pour
correspondre au jeu de données.

Les deux types de configurations (adaptation à l’environnement et paramètres de l’analyse) sont
stockés dans deux fichiers séparés dans le dossier _scripts_ :

Fichier **_cluster.yaml_**

Ce fichier contient un patron que suivront les différents jobs lancés sur le cluster par Snakemake. Si le
script a été installé via la commande au-dessus, la version téléchargée devrait être directement
adaptée pour le cluster CIRAD. Sinon (ou si utilisé sur un autre cluster) il faudra le modifier pour qu’il
corresponde à la nomenclature et règles du cluster :
```console
nano scripts/cluster.yaml
```


Le principal détail à adapter sera les noms des différentes queues (agap_normal, agap_short et
agap_long pour le cluster Muse du CIRAD)

Fichier **_config.yaml_**

Ce fichier contient les paramètres liés à l’analyse.
```console
nano scripts/config.yaml
```

Ce fichier contient, dans l’ordre d’apparition dans le fichier :

- Le chemin vers le dossier contenant les **données** (DATADIR)
- **L’extension des fichiers de lectures**. Remplir ici la fin du nom des fichiers d’entrée qui
    différencie les lectures forward et reverse ; le reste du nom servira d’identifiant
    d’échantillon. Si plusieurs échantillons doivent être analysées simultanément, ces extensions
    doivent être communes à tous les échantillons. Exemple pour des fichiers
    U005_T1_R1_001.fastq.gz et U005_T1_R2_001.fastq.gz, indiquer

```console
ext1:"_R1_001.fastq.gz"

ext2:"_R2_001.fastq.gz"
```

Le reste du nom du fichier U005_T1 sera automatiquement attribué à cet échantillon.
```console
refgen: "ref.fa"
```
- Le chemin (et le nom) vers le **génome de référence** à utiliser pour le premier round
    d’assemblage . Par défaut la séquence est cherchée dans le dossier DATADIR , ici ref.fa
- Les **paramètres d’assemblage** (je conseille de laisser les deux derniers aux valeurs par
    défaut, mais on peut modifier le premier paramètre : couverture minimum par site).

```console
refid: "NC_006383.2"
```
Ici, il est possible de modifier l'identifiant GenBank de la référence qui sera utilisée pour l'annotation des SNP.


### Test_script et run_script

Dans le dossier principal PPRreadds_ASSEMBLY, deux fichiers permettent de lancer l’analyse :
**test_run.sh** et **run_snake.sh**

- test_run.sh utilise le gestionnaire de pipeline pour détecter les échantillons à analyser et
    prévoir quels jobs lancer. Son utilisation est optionnelle mais permet de s’assurer que les
    données sont au bon format, que les chemins sont corrects et que l’extension des fichiers de
    lectures est bien renseignée. **Aucune analyse n’est lancée**.
- run_snake.sh permet de lancer l’analyse sur les échantillons détectés. Il peut être utilisé
    directement :
```console
./run_script.sh
```
il est également possible d’utiliser un job pour l’envoyer sur un nœud :
```console
sbatch -A agap - p agap_short --nodes=1 --wrap ‘./run_script.sh’
```

## 4. Fonctionnement du pipeline

Toutes les opérations effectuées par le script sont décrites dans le diagramme ci-dessous (généré automatiquement par snakemake !!) :

<p style="text-align: center"><img src="/dag.svg"></p>

J’ai essayé de rendre les noms d’opérations aussi parlants (mais courts) que possible. Voici une
description légèrement plus détaillée :

- **cleanReads** : nettoyage des lectures en retirant les séquences trop courtes (comme spécifié
    dans scripts/config.yaml)
- **BWAindexRef** : indexage du génome de référence avant l’assemblage
- **BWAmemRef** : Premier round d’assemblage des lectures nettoyées sur le génome de
    référence
- **samtoolsConsensus** : Création du consensus du premier round d’assemblage
- **BWAindexCons** : indexage du premier consensus avant le second round d’asssemblage

- **BWAmemCons** : Second round d’assemblage, en utilisant le premier consensus comme
    référence
- **samtoolsConsensus 2** : Création du consensus du second round d’assemblage
- **assemblyStats** : Calcul et écriture des statistiques d’assemblage et de couverture
- **sam2bam** : conversion des fichiers .sam en .bam pour économiser l’espace disque

- **sam2bam** : conversion des fichiers .sam en .bam pour économiser l’espace disque

- **lofreq** : appel de variants pour inférer les SNV et les indels à partir de données de séquençage de nouvelle génération

- **snpEff_to_tsv** : transforme le vcf en tsv

- **generate_manhattan_plots** : Plot les snp selon leur annotation

Je copie également ici la documentation des statistiques d’assemblage :

rname Reference name / chromosome
startpos Start position
endpos End position (or sequence length)
numreads Number reads aligned to the region (after filtering)
covbases Number of covered bases with depth >= 1
coverage Percentage of covered bases [0..100]
meandepth Mean depth of coverage
meanbaseq Mean baseQ in covered region
meanmapq Mean mapQ of selected reads

Un fichier de sortie détaillé de chaque opération est sauvegardé dans le dossier **Logs** , et porte un
nom associant l’échantillon et l’opération (par ex. CamerounAnimalN2_S6_L001_cleanReads.log).

## 5. Fichiers en sortie

Les principaux fichiers intéressants en sortie de script sont réunis dans le dossier **Results/** :

- Le **fichier {sample}_consensus2.fasta** contient le consensus du second round d’assemblage
    au format fasta.
- Le fichier **{sample}_mapping.stats** contient les statistiques d’assemblages (voir ci-dessus).

Les fichiers intermédiaires peuvent être retrouvés dans le sous-dossier **Results/Intermediary_files/** :

- **{sample}_R1_clean.fastq.gz** et **{sample}_R 2 _clean.fastq.gz** contiennent les lectures filtrées
    (lectures trop courtes retirées)
- **{sample}_reads_cleaning.html** contient un résumé visuel de l’étape de nettoyage des
    lectures, qui peut être téléchargé et ouvert avec un navigateur internet
- **{sample}_consensus 1 .fasta** est le consensus du premier round d’assemblage
- **{sample}_mapping_to_ref_sort.bam** est le fichier.bam du premier round d’assemblage
- **{sample}_mapping_to_cons_sort.bam** est le fichier.bam du second round d’assemblage
- **{sample}_ann.vcf**  est le fichier.vcf annoté des variations de séquences génétiques
- **{sample}_effect.pdf**  est le fichier.pdf des plot pour les impact/effet des variants
