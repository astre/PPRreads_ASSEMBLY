#/bin/bash
## Simple wrapper to running the Map_to_reference snakemake script.

# Load latest version of snakemake installed on the cluster
module load snakemake/7.15.1-conda

# Create the skeleton diretory architecture
if ! [ -d Results/Intermediary_files ] ; then mkdir -p Results/Intermediary_files ; fi
if ! [ -d Logs ] ; then mkdir -p Logs ; fi

# Snakemake command
snakemake -s scripts/Map_to_reference.snake --keep-going --cores 1 --use-conda --configfile=scripts/config.yaml --conda-frontend conda --cluster-config scripts/cluster.yaml --cluster "sbatch -J {cluster.job-name} -A agap -p {cluster.queue} {cluster.mem} {cluster.time} {cluster.nodes} {cluster.cpus} {cluster.out}" --jobs 100 --printshellcmds --rerun-incomplete
