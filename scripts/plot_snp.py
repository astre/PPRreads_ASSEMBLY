import pandas as pd                  # Import pandas library for data manipulation
import matplotlib.pyplot as plt      # Import matplotlib.pyplot for plotting
import seaborn as sns                # Import seaborn for color palette
import sys, os                           # Import sys for command-line argument parsing
from Bio import SeqIO                # Import SeqIO from Biopython for GenBank file parsing
from matplotlib.backends.backend_pdf import PdfPages  # Import PdfPages from matplotlib for saving the plot as a PDF

if len(sys.argv) != 4:               # Check if the correct number of command-line arguments is provided
    print("Usage: python script.py <tsv_file> <gbk_file> <output_name>")
    sys.exit(1)

tsv_file = sys.argv[1]               # Assign the first command-line argument to tsv_file
gbk_file = sys.argv[2]               # Assign the second command-line argument to gbk_file
output_name = sys.argv[3]            # Assign the third command-line argument to output_name

# Load data from the TSV file
df = pd.read_csv(tsv_file, sep='\t')
df['ANN[*].EFFECT'] = df['ANN[*].EFFECT'].replace({'downstream_gene_variant': 'upstream/downstream_gene_variant', 'upstream_gene_variant': 'upstream/downstream_gene_variant'})

# Calculate frequency (AO/DP)
df['Frequency'] = df['AF']

# Convert the 'EFFECT' column to categorical type
df['EFFECT'] = df['ANN[*].EFFECT'].astype('category')

# Create a color palette for the effects
color_palette = sns.color_palette('hls', n_colors=len(df['EFFECT'].cat.categories))

# Parse the GenBank file and extract gene information
genes = {}
genome_positions = []

for record in SeqIO.parse(gbk_file, "genbank"):
    genome_positions.append(record.features[0].location.start)
    genome_positions.append(record.features[-1].location.end)
    for feature in record.features:
        if feature.type == "gene":
            start = feature.location.start
            end = feature.location.end
            gene_name = feature.qualifiers['gene'][0]
            genes[(start, end)] = gene_name

min_position = min(genome_positions)
max_position = max(genome_positions)

# Create a PdfPages object for saving the plot as a PDF
pp = PdfPages(f'{output_name}')

# Create a single figure with multiple axes
fig, axs = plt.subplots(len(df['EFFECT'].cat.categories), 1, figsize=(12, 6 * len(df['EFFECT'].cat.categories)))

# Iterate through each effect category and create a plot on a separate axis
for i, (effect, color) in enumerate(zip(df['EFFECT'].cat.categories, color_palette)):
    # Filter the DataFrame based on the effect
    effect_df = df[df['ANN[*].EFFECT'] == effect]

    # Plot a scatter plot for the points
    axs[i].scatter(effect_df['POS'], effect_df['Frequency'], color=color)

    # Plot a line for each point
    for _, row in effect_df.iterrows():
        axs[i].plot([row['POS'], row['POS']], [0, row['Frequency']], color=color, linestyle='dashed', linewidth=0.5)

    # Add gene annotations with brackets, excluding "C" and "V" genes
    for (start, end), gene_name in genes.items():
        if gene_name not in ['C', 'V']:
            # Adjust the position of the brackets and gene labels
            axs[i].annotate('', xy=(start, -0.07), xytext=(end, -0.07), xycoords='data', textcoords='data',
                            arrowprops=dict(arrowstyle='<|-|>', color='black', linewidth=1))
            axs[i].text((start + end) / 2, -0.1, gene_name, ha='center', fontsize=8)

            # Color the gene region in gray
            axs[i].axvspan(start, end, color='lightgrey', alpha=0.3)

    # Set the x-axis limits to include the entire genome
    axs[i].set_xlim(1, max_position)
    axs[i].spines['bottom'].set_position('zero')

    # Set the y-axis limits based on the minimum and maximum values of the 'Frequency' column
    axs[i].set_ylim(bottom=-0.1, top=effect_df['Frequency'].max() + 0.1)  # Set the lower limit to 0

    # Add a title
    axs[i].set_title(f'{effect}')

# Adjust the spacing between subplots
plt.tight_layout()

# Save the plot as a PDF
pp.savefig()

# Close the PDF file
pp.close()
