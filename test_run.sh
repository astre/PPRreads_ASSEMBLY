#!/bin/bash
# Simple wrapper to run a snakemake dry-run with current parameters;

if module list -t 2>&1 | grep -q snakemake ; then loaded="yes" ; else loaded="no" ; fi
module load snakemake/7.15.1-conda

snakemake -s scripts/Map_to_reference.snake --keep-going --cores 1 --use-conda --configfile=scripts/config.yaml --conda-frontend mamba --jobs 100 --printshellcmds --rerun-incomplete

if [ $loaded == "no" ] ; then module unload snakemake ; fi
